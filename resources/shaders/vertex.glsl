#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal_modelspace;

// Output data
out vec2 UV;
out vec3 Position_worldspace;
out vec3 Normal_cameraspace;
out vec3 EyeDirection_cameraspace;
out vec3 LightDirection_cameraspace;

// Values that stay constant for the whole mesh
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;
uniform vec3 LightPosition_worldspace;

void main(){

    // Output position of the vertex, in clip space : MVP * position
    gl_Position = MVP * vec4(vertexPosition_modelspace,1);

	// position of the vertex in worldspace
	Position_worldspace = (M * vec4(vertexPosition_modelspace,1)).xyz;

	// vector from vertex to camera, in camera space 
	// (camera at 0,0,0 in camera space)
	vec3 vertexPosition_cameraspace = (V * M * vec4(vertexPosition_modelspace, 1)).xyz;
	EyeDirection_cameraspace = vec3(0, 0, 0) - vertexPosition_cameraspace;

	// vector from vertex to light, in camera space
	vec3 LightPosition_cameraspace = ( V * vec4(LightPosition_worldspace, 1)).xyz;
	LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;

	// Normal of the vertex, in camera space
	// only correct if model matrix does not scale model (why?)
	Normal_cameraspace = ( V * M * vec4(vertexNormal_modelspace,0)).xyz;

	// UV of vertex
	UV = vertexUV;
}
