#version 330 core

// from vertex shader
in vec2 UV;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;

// Ouput data
layout(location = 0) out vec4 color;

// values which stay constant for a whole mesh
uniform sampler2D myTextureSampler;
uniform mat4 MV;
uniform vec3 LightPosition_worldspace;

void main()
{
	// Light emission properties
	vec3 LightColor = vec3(1, 1, 1);
	float LightPower = 100.0f;

	// Material properties
	vec3 MaterialDiffuseColor = texture2D(myTextureSampler, UV).rgb;
	// AMBIENT LIGHT IS A HACK - it makes sure unlit portions of objects aren't black
	vec3 MaterialAmbientColor = vec3(0.6, 0.6, 0.6) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3, 0.3, 0.3); // originally .3 .3 .3

	// Distance to light
	float distance = length(LightPosition_worldspace - Position_worldspace);

	// Normal of the computed fragment, in camera space
	vec3 n = normalize(Normal_cameraspace);
	// direction of light
	vec3 l = normalize(LightDirection_cameraspace);

	// light is at vertical -> 1
	// light is perpedicular -> 0
	// light is behind -> 0
	float cosTheta = clamp(dot(n, l), 0, 1); // clamped between 0 and 1

	// Eye vector
	vec3 E = normalize(EyeDirection_cameraspace);
	vec3 R = reflect(-l, n);

	float cosAlpha = clamp(dot(E, R), 0, 1);

	// Output color = color specified at UV
	color = 
		vec4 (
		// AMBIENT (THIS IS A HACK)
		MaterialAmbientColor +
		// DIFFUSE
		MaterialDiffuseColor * LightColor * LightPower * cosTheta / (distance*distance) +
		// SPECULAR
		MaterialSpecularColor * LightColor * LightPower * pow(cosAlpha,5) / (distance*distance),
		1.0);

}
