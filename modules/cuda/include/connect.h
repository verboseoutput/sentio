#ifndef CONNECT_H
#define CONNECT_H

#include <glbinding/gl/types.h>

enum class bufferType : unsigned char {
    READ, 
    WRITE, 
    READWRITE
};

void registerBufferWithCuda(gl::GLuint buffer, bufferType type);
void updateHostArray(unsigned char* dst, int width, int height);

#endif