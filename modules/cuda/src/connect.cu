
#include "connect.h"

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <cuda_profiler_api.h>
#include <iostream>
#include "helper_cuda.h"

void flipBuffer(cudaArray* in_arr, unsigned int* out_buffer, int width, int height);

// cuda graphics resource which points to the color buffer we render to
// in our frame buffer object
struct cudaGraphicsResource *cuda_color_buffer;
texture<uchar4, 2, cudaReadModeElementType> inTex;

unsigned int* dev_output_buffer;

void registerBufferWithCuda(gl::GLuint buffer, bufferType type) {
    uint flag;
    switch(type) {
        case bufferType::READ:
            flag = cudaGraphicsMapFlagsReadOnly;
            break;
        case bufferType::WRITE:
            flag = cudaGraphicsMapFlagsWriteDiscard;
            break;
        case bufferType::READWRITE:
            flag = cudaGraphicsMapFlagsNone;
            break;
        default:
            break;
    }
    
    checkCudaErrors(cudaGraphicsGLRegisterImage(
        &cuda_color_buffer, buffer, GL_TEXTURE_2D, flag));

    int num_values = 512*512*4;
    checkCudaErrors(cudaMalloc((void **)&dev_output_buffer, sizeof(GLubyte) * num_values));
}

void updateHostArray(unsigned char* cpu_buffer, int width, int height) {
    // get the color buffer data
    cudaArray *color_array;
    checkCudaErrors(cudaGraphicsMapResources(1, &cuda_color_buffer, 0));
    checkCudaErrors(cudaGraphicsSubResourceGetMappedArray(
        &color_array, cuda_color_buffer, 0, 0));

    // our color array is currently mirror flipped vertically
    // because opengl sets 0,0 as the bottom left corner, so we flip the data
    // and put it into our device output buffer
    flipBuffer(color_array, dev_output_buffer, width, height);
    
    // move our final device data (dev_output_buffer) to the cpu
    checkCudaErrors(cudaMemcpy2D( 
        cpu_buffer,         // destination
        width*4,              // width of destination (dpitch)
        dev_output_buffer,  // source memory address
        width*4,                // source pitch
        width*4,              // width of source in bytes
        height,                // height (rows)
        cudaMemcpyDeviceToHost   // type of transfer (default means figure it out based on
                            // source and destination)
        ));


    checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_color_buffer, 0)); 
}

__global__ void
verticalFlipKernel(unsigned int *out_data, int imgw, int imgh) {
    // determine which part of the array this kernel is working on
    int tx = threadIdx.x;
    int ty = threadIdx.y;
    int bw = blockDim.x;
    int bh = blockDim.y;
    int x = blockIdx.x*bw + tx;
    int y = blockIdx.y*bh + ty;

    uchar4 ucres = tex2D(inTex, x, y);
    

    out_data[imgw*(imgw-y-1) + x] = 
        // r | g | b | a
        ucres.x | (ucres.y<<8) | (ucres.z<<16) | 0xFF000000 ;
}


void flipBuffer(cudaArray* in_arr, unsigned int* out_buffer, int width, int height) {
    // calculate grid size
    dim3 block(16, 16, 1);
    dim3 grid(width / block.x, height / block.y, 1);

    checkCudaErrors(cudaBindTextureToArray(inTex, in_arr));

    struct cudaChannelFormatDesc desc;
    checkCudaErrors(cudaGetChannelDesc(&desc, in_arr));

    verticalFlipKernel<<< grid, block>>>(out_buffer, width, height);
}