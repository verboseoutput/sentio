#include <fstream>
#include <iostream>
#include <iomanip>

#include "constants.h"
#include "Log.h"

// needed for MSVC
#ifdef WIN32
#define localtime_r(_Time, _Tm) localtime_s(_Tm, _Time)
#endif // localtime_r

namespace lg {

Log::Log() {
    m_fileBuf.open(LOG_DIR+"lastRun.html", std::fstream::out | std::fstream::trunc);
    bool isopen = m_fileBuf.is_open();
    m_outputStream = new std::ostream(&m_fileBuf);

    m_levelString[LogLevel::Info] = "Info";
    m_levelString[LogLevel::Warning] = "Warning";
    m_levelString[LogLevel::Error] = "Error";

    m_minLevel = LogLevel::Info;

    m_mutex.lock();
    updateLocalTime();
	*m_outputStream
        << "<!DOCTYPE html>\n"
        << "<html>\n"
        << "<head>\n"
        << "<title>Sentio</title>\n"
        << "<link rel=\"stylesheet\" type=\"text/css\" href=\"" << STYLE_DIR << "LogStyle.css\">"
        << "</head>\n"
        << "<body class=\"runLog\">\n\n"
        << "<h1 class=\"logHeading\">Sentio Run Log</h1>"
        << "<h3 class\"logHeading\">" << std::put_time(&m_now.local_time, "%b %e %Y") << "</h3>\n\n"
        << "<table class=\"logTable\">"
        << std::endl;
    m_mutex.unlock();
}

Log::~Log() {
    *m_outputStream
        << "</table>\n\n"
        << "</body>\n"
        << "</html" << std::endl;

    m_outputStream->flush();
    delete(m_outputStream);
    m_outputStream = nullptr;
    
    m_fileBuf.close();
}



void Log::updateLocalTime() {
    auto now = std::chrono::high_resolution_clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) -
        std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());

    m_now.ms = ms.count();
    
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	localtime_r(&in_time_t, &m_now.local_time);
}

Log& Log::get() {
    static Log instance;
    return instance;
}

void Log::log(LogLevel level, std::string msg) {
    if(level < m_minLevel) return;

    m_mutex.lock();
    updateLocalTime();

	*m_outputStream
        << "<tr class=\"logTr\">"
        
        << "<td class=\"time\">"
        << "[" << std::put_time(&m_now.local_time, "%H:%M:%S.") 
        << std::setfill('0') << std::setw(3) << m_now.ms << "]"
        << "</td>\n"
        
        << "<td class=\"Severity " << m_levelString[level] << "\">"
		<< m_levelString[level] << ":"
        << "</td>\n"

        << "<td>"
		<< msg 
        << "</td>\n";
	m_mutex.unlock();
}

LogStream Log::operator()() {
    return LogStream(*this, LogLevel::Error);
}

LogStream Log::operator()(LogLevel level) {
    return LogStream(*this, level);
}

void Log::setMinLevel(LogLevel level) {
    m_minLevel = level;
}

LogStream::LogStream(Log& log, LogLevel level) :
m_log(log), m_level(level)
{}

LogStream::LogStream(const LogStream& ls) :
m_log(ls.m_log), m_level(ls.m_level) {}

LogStream::~LogStream() {
	m_log.log(m_level, str());
}

// instantiate our globally accessible extern variables
// defined at the end of the Log.h file
Log& log = Log::get();

LogLevel info = LogLevel::Info;
LogLevel warning = LogLevel::Warning;
LogLevel error = LogLevel::Error;

} // end of namespace lg