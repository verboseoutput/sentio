#ifndef LOG_H
#define LOG_H

#include <string>
#include <sstream>
#include <mutex>
#include <ostream>
#include <fstream>
#include <unordered_map>

#include "MyUnorderedMap.h"

/**
 * created several classes with very generic names (log)
 * a namespace seemed appropriate
 */
namespace lg {

// tm doesn't save millisecond resolution, so creating a struct
// not for exact profiling, just to give a sense of execution times
struct now_t {
    tm local_time;
    int ms;
};

// how important is this log message
enum class LogLevel { Info, Warning, Error };

class Log;

/**
 * a string stream object which also has a handle to our log
 * and knows what level the message is at
 * 
 * calls Logs log(LogLevel, msg) function when it goes out of scope
 */
class LogStream : public std::ostringstream {
    Log& m_log;
	LogLevel m_level;

public:
    LogStream(Log& log, LogLevel logLevel);
	LogStream(const LogStream& ls);
	~LogStream();
};

/**
 * singleton pattern log function with multi threaded protection
 * log is an html file currently saved at logs/lastRun.html
 * 
 * builds up a message using LogStream, than writes the message to the
 * log via log(LogLevel, msg)
 * 
 * access the instance of Log via Log::get()
 * pass a message to it via  Log::get()(optional log level) << "your message"
 * set minimum log level via setMinLevel(LogLevel)
 */
class Log {
    Log();
    ~Log();
    void updateLocalTime();

    now_t m_now;
    MyUnorderedMap< LogLevel, std::string> m_levelString;
    std::mutex m_mutex;
    std::ostream* m_outputStream;
    std::filebuf m_fileBuf;
    LogLevel m_minLevel;
public:
    Log(Log const&) = delete;
    void operator=(Log const&) = delete;

    static Log& get();

    void log(LogLevel level, std::string msg);
    LogStream operator()();
    LogStream operator()(LogLevel level);
    void setMinLevel(LogLevel level);

};


/**
 * Used extern variables so that the log can be accessed
 * like this: lg::log() <<
 * rather than like this: lg::Log::get()() <<
 * similarly for indicating message level lg::info rather than lg::LogLevel::Info
 * 
 * variables are instantiated at the end of the Log.cpp file
 */
extern Log& log;

extern LogLevel info;
extern LogLevel warning;
extern LogLevel error;

} // end of namespace lg

#endif