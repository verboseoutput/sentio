cmake_minimum_required(VERSION 3.1)
project (sentio)
if (CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_CURRENT_BINARY_DIR)
    message(FATAL_ERROR "You must set your binary directory different from your source")
endif()

# set some variables
set( EXE                            "sentioMain"            )
set( BIN_DIR                        ${CMAKE_SOURCE_DIR}/bin )
set( CMAKE_CXX_STANDARD             11                      )
set( CMAKE_CXX_STANDARD_REQUIRED    ON                      )
set( CMAKE_CXX_EXTENSIONS           OFF                     )
set( RESOURCE_DIR					${CMAKE_SOURCE_DIR}/resources )
set( LOG_DIR                        ${CMAKE_SOURCE_DIR}/logs)
set( CMAKE_MODULE_PATH              ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/modules/")

# Load Modules
include(CMakeToolsHelpers OPTIONAL) # helps while using VS Code
include(ExternalProject)            # allows custom build commands for external projects

# find required libraries already on the system
# assimp
find_package(assimp REQUIRED)
set(THIRDPARTY_INCLUDE_DIRS ${THIRDPARTY_INCLUDE_DIRS} ${assimp_INCLUDE_DIRS})
set(THIRDPARTY_LIBS ${THIRDPARTY_LIBS} ${assimp_LIBRARIES})

# SDL2
set(SDL_SHARED_ENABLED_BY_DEFAULT OFF) 
find_package(SDL2 REQUIRED)
set(THIRDPARTY_INCLUDE_DIRS ${THIRDPARTY_INCLUDE_DIRS} ${SDL2_INCLUDE_DIR})
set(THIRDPARTY_LIBS ${THIRDPARTY_LIBS} ${SDL2_LIBRARY})

# glbinding
find_package(glbinding REQUIRED)
set(THIRDPARTY_LIBS ${THIRDPARTY_LIBS} glbinding::glbinding)

# glm
find_package(glm REQUIRED)
set(THIRDPARTY_INCLUDE_DIRS ${THIRDPARTY_INCLUDE_DIRS} ${GLM_INCLUDE_DIRS})

# SOIL
ExternalProject_Add(SOIL
    SOURCE_DIR ${CMAKE_SOURCE_DIR}/lib/SOIL/src
    BINARY_DIR ${CMAKE_SOURCE_DIR}/lib/SOIL/projects/makefile
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${MAKE}
    INSTALL_COMMAND "")
set(THIRDPARTY_INCLUDE_DIRS ${THIRDPARTY_INCLUDE_DIRS} "lib/SOIL/src")
set(THIRDPARTY_LIBS ${THIRDPARTY_LIBS} "${CMAKE_SOURCE_DIR}/lib/SOIL/lib/libSOIL.a")

# cuda common headers
set(THIRDPARTY_INCLUDE_DIRS ${THIRDPARTY_INCLUDE_DIRS} "lib/cuda/inc/")

# include all or the directories from the third party libraries
include_directories(${THIRDPARTY_INCLUDE_DIRS})

# my cuda module
add_subdirectory("modules/cuda" "${CMAKE_BINARY_DIR}/modules/cuda")
set(THIRDPARTY_INCLUDE_DIRS ${THIRDPARTY_INCLUDE_DIRS} "modules/cuda/include")
set(THIRDPARTY_LIBS ${THIRDPARTY_LIBS} myCuda)

add_subdirectory("modules/utilities" "${CMAKE_BINARY_DIR}/modules/utilities")
set(THIRDPARTY_INCLUDE_DIRS ${THIRDPARTY_INCLUDE_DIRS} "modules/utilities/include")

# add my subdirectories
add_subdirectory(${CMAKE_SOURCE_DIR}/main)

# make sure the binary is actually built in the binary directory
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${BIN_DIR} )

add_executable( ${EXE} ${SOURCES} ${HEADERS})
include_directories( ${INCLUDE_DIRS} )
target_link_libraries( ${EXE} ${THIRDPARTY_LIBS} )
