#ifndef WORLD_OPTIONS_H
#define WORLD_OPTIONS_H

struct WorldOptions {
    int renderWidth;
    int renderHeight;
};

#endif