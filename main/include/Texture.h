#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <glbinding/gl/types.h>

/**
 * loads and stores a texture from file.
 **/
class Texture {
    gl::GLuint m_textureHandle;
    std::string m_fileName;

public:
    void setFileName(std::string name) {m_fileName = name; }
    gl::GLuint getId() {return m_textureHandle; }
    bool generateTexture();
};

#endif