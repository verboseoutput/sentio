#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <string>
#include <memory>

#include <assimp/Importer.hpp>
#include <glm/glm.hpp>
#include <glbinding/gl/types.h>

// forward declerations
struct aiMesh;

/**
 * contains all data related to an objects mesh data as well as functions
 * to load the data and setup render buffers.
 **/
class Model {
public:
    enum Data : char {VERTEX, UV, NORMAL, ELEMENT, COUNT};
    
    void setPath(std::string filepath) { m_path = filepath; }
    bool loadModelData(Assimp::Importer& importer);
    void setupRenderBuffer();
    gl::GLuint getBuffer(Data data);
    int getSize();

private:
    std::vector<uint16_t> indices;
    std::vector<glm::vec3> indexedVertices;
    std::vector<glm::vec2>	indexedUVs;
    std::vector<glm::vec3> indexedNormals;

    std::string m_path;
    gl::GLuint m_bufferData[Data::COUNT];

    void setMeshData(const aiMesh* mesh);
};

#endif // !SCENE_OBJECT_H_