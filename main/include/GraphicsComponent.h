#ifndef GRAPHICS_COMPONENT_H
#define GRAPHICS_COMPONENT_H

#include "Component.h"

class Renderer;
class Model;
class Texture;
class AgentEntity;

/**
 * Component in charge of all aspects of an entities visual appearance
 **/
class GraphicsComponent : public Component {
    const Renderer& m_renderer;
    Model& m_model;
    Texture& m_texture;

public:
    GraphicsComponent(const Renderer& renderer, Model& model, Texture& texture) :
        m_renderer(renderer),
        m_model(model),
        m_texture(texture) {}
    void update(Entity& entity, unsigned int ticksPerFrame);
};

#endif