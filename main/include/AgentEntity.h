#ifndef AGENT_ENTITY_H
#define AGENT_ENTITY_H

#include <atomic>

#include <glm/glm.hpp>
#include <SDL.h>

#include "Entity.h"

/** 
 * The 'body' of our agent. Also acts as the camera location. A specialized
 * entity which computes projection and view matrices based on its position
 * and direction.
 **/
class AgentEntity : public Entity {
    glm::mat4 m_projectionMatrix;
    glm::mat4 m_viewMatrix;
    int m_width;
    int m_height;
    
public:
    AgentEntity();
    ~AgentEntity();

    glm::mat4 getProjectionMatrix() const { return m_projectionMatrix; }
    glm::mat4 getViewMatrix() const { return m_viewMatrix; }
    void computeMatrices();
    unsigned char* backBuffer();
    void bufferUpdated();
    SDL_Surface* getView();
    void setupInput(int width, int height);
    int inputWidth() { return m_width; }
    int inputHeight() { return m_height; }

    void run(std::atomic<bool>& running);
};

#endif