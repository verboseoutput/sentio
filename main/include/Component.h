#ifndef COMPONONENT_H
#define COMPONONENT_H

class Entity;

/**
 * A part of an entity which needs updating while the 'world' is in its
 * run state.
 * 
 * allows us to create entities with mix and match components for example:
 * A decoration, with a graphics component but no physics. 
 * A zone of effect with physics and no graphics.
 * An entity, with graphics and physics components.
 * 
 * Based on http://gameprogrammingpatterns.com/component.html
 **/
class Component {
public:
    enum class Type : char {
        graphics,
        input
    };
    
    virtual ~Component() {}
    virtual void update(Entity& entity, unsigned int ticksPerFrame) = 0;
};

#endif