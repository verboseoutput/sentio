#ifndef SHADER_H
#define SHADER_H

#include <string>

#include <glbinding/gl/gl.h>

/**
 * loads and compiles a shader program
 **/
gl::GLuint compileShaderProgram(const char* vertexFilePath, const char* fragmentFilePath);


#endif