#ifndef RENDERER_H
#define RENDERER_H

#include <glbinding/gl/types.h>

#include "AgentEntity.h"

struct shader_ids {
	gl::GLuint programId;
	gl::GLuint matrixId;
	gl::GLuint viewMatrixId;
	gl::GLuint modelMatrixId;
	gl::GLuint textureId;
	gl::GLuint lightId;
};

/**
 * initializes rendering properties. sets up each frame for rendering
 **/
class Renderer {
	gl::GLuint m_vertexArrayId;
	gl::GLuint m_colorBuffer;
	gl::GLuint m_depthBuffer;
	gl::GLuint m_frameBufferObj;
	shader_ids m_shaderIds;
	AgentEntity* m_camera;

	gl::GLuint m_fullScreenVertexBuffer;
	gl::GLuint m_fullScreenShaderId;
	gl::GLuint m_fullScreenTextId;

	bool setupOffscreenRendering();

public:
	bool init(AgentEntity& camera);
	void setupFrame() const;
	glm::mat4 getMVP(glm::mat4& modelMatrix) const;
	shader_ids getShaderIds() const { return m_shaderIds; }
	void displayFBO() const;
};

#endif
