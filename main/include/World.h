#ifndef LEVEL_H
#define LEVEL_H

#include <string>
#include <list>
#include <cstdint>
#include <memory>

#include <glbinding/gl/types.h>

#include "MyUnorderedMap.h"
#include "Entity.h"
#include "AgentEntity.h"
#include "Renderer.h"
#include "WorldOptions.h"

class Agent;
class Model;
class Texture;
class SDL_Window;

/**
 * Defines all aspects of the world used to test the agent.
 * 
 * loads models and textures
 * sets up all world entities. (models, texture, position, etc...)
 * run loop advances the world state
 * checks for win condition by the agent
 **/
class World {
	bool handleEvents();
	void mySleep(uint32_t msecs);

	bool createEntities();
	void displayVisualBuffer(unsigned char* buffer, SDL_Window* window);

	WorldOptions& m_options;
	AgentEntity m_agentEntity;
	Renderer m_renderer;

	MyUnorderedMap<std::string, std::unique_ptr<Model>> m_models;
	MyUnorderedMap<std::string, std::unique_ptr<Texture>> m_textures;
    std::list<std::unique_ptr<Component>> m_components;
	std::list<std::unique_ptr<Entity>> m_entities;

public:
	World(WorldOptions& options) :
		m_options(options) {}
	~World();
	bool loadFromScript(const char* scriptName);
	bool init();
	void run(SDL_Window* window);
	void cleanup();
};

#endif // !LEVEL_H_
