#ifndef ENTITY_H
#define ENTITY_H

#include <unordered_map>
#include <memory>

#include <glm/glm.hpp>

#include "MyUnorderedMap.h"
#include "Component.h"
#include "Texture.h"
#include "Model.h"

/**
 * Something which exists in the world. contains a position and list of
 * components which need to be updated every frame. Components are stored
 * in an unordered map to prevent us from accidentaly adding more than one
 * component of the same type to an entity. 
 * 
 * TODO: direction is seperate from an objects rotation, which isn't necessary
 **/
class Entity {
protected:
    glm::vec3 m_position;
	glm::vec3 m_scale;
	glm::vec3 m_rotation;
    // direction the entity is facing in radians
    float m_horizontalAngle;
    float m_verticalAngle;

    MyUnorderedMap<Component::Type, std::shared_ptr<Component>> m_components;

public:
    Entity() :
        m_horizontalAngle(0),
        m_verticalAngle(0) {}
    void setPosition(glm::vec3 pos);
    void setScale(glm::vec3 scale);
    void setRotation(glm::vec3 rotation);

    glm::vec3 getPosition();
    glm::vec3 getScale();
    float getXRotation();
	float getYRotation();
	float getZRotation();

    void addComponent(Component::Type type, std::shared_ptr<Component> component);
    
    void update(unsigned int ticksPerFrame);

    void rotate(glm::vec3 rotationVector);

};

#endif