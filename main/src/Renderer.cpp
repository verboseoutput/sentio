#include "Renderer.h"

#include <iostream>

#include <glbinding/gl/gl.h>
#include <glbinding/gl/types.h>
#include <glbinding/Binding.h>
#include <glm/glm.hpp>

#include "shader.h"
#include "connect.h"
#include "Log.h"



using namespace gl;
bool Renderer::init(AgentEntity& camera) {
	m_camera = &camera;

	// setup openGL resources
	glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	// setup VAO
	glGenVertexArrays(1, &m_vertexArrayId);
	glBindVertexArray(m_vertexArrayId);

	// Build and compile our shader program
	m_shaderIds.programId = compileShaderProgram("vertex.glsl", "fragment.glsl");
	if (m_shaderIds.programId == -1) {
		lg::log() << "shader creation failed\n";
		return false;
	}

	m_shaderIds.matrixId = glGetUniformLocation(m_shaderIds.programId, "MVP");
	m_shaderIds.viewMatrixId = glGetUniformLocation(m_shaderIds.programId, "V");
	m_shaderIds.modelMatrixId = glGetUniformLocation(m_shaderIds.programId, "M");

	m_shaderIds.textureId = glGetUniformLocation(m_shaderIds.programId, "myTextureSampler");

	m_shaderIds.lightId = glGetUniformLocation(m_shaderIds.programId, "LightPosition_worldspace");
	
	

	if(!setupOffscreenRendering()) {
		return false;
	}
	registerBufferWithCuda(m_colorBuffer, bufferType::READ);

	// For use in displaying the rendered texture
	static const GLfloat g_fullscreen_vertex_buffer_data[] = { 
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f,
	};

	glGenBuffers(1, &m_fullScreenVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_fullScreenVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_fullscreen_vertex_buffer_data), g_fullscreen_vertex_buffer_data, GL_STATIC_DRAW);

	m_fullScreenShaderId = compileShaderProgram("passThroughVertex.glsl", "boringFragment.glsl");
	m_fullScreenTextId = glGetUniformLocation(m_fullScreenShaderId, "renderedTexture");

	return true;
}

void Renderer::setupFrame() const {	
	// bind the frame buffer object so that we render to it rather than to the screen
	glBindFramebuffer(GL_FRAMEBUFFER, m_frameBufferObj);
	GLenum attachments[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, attachments);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(m_shaderIds.programId);

	glm::vec3 lightPos = glm::vec3(3,0,4);
	glUniform3f(m_shaderIds.lightId, lightPos.x, lightPos.y, lightPos.z);

	glm::mat4 viewMatrix = m_camera->getViewMatrix();
	glUniformMatrix4fv(m_shaderIds.viewMatrixId, 1, GL_FALSE, &viewMatrix[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(m_shaderIds.textureId, 0);
}

glm::mat4 Renderer::getMVP(glm::mat4& modelMatrix) const { 
	return m_camera->getProjectionMatrix() *
		m_camera->getViewMatrix() *
		modelMatrix;
}

bool Renderer::setupOffscreenRendering() {
	// create color buffer
    glGenTextures(1, &m_colorBuffer);
    glBindTexture(GL_TEXTURE_2D, m_colorBuffer);

    // set basic parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // buffer data
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_camera->inputWidth(), m_camera->inputHeight(),
		0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	// create depth buffer
	glGenRenderbuffersEXT(1, &m_depthBuffer);
    glBindRenderbufferEXT(GL_RENDERBUFFER, m_depthBuffer);

    // allocate storage
    glRenderbufferStorageEXT(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, m_camera->inputWidth(), m_camera->inputHeight());

	// create frame buffer object
    glGenFramebuffers(1, &m_frameBufferObj);
    glBindFramebuffer(GL_FRAMEBUFFER, m_frameBufferObj);

    // attach images
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_colorBuffer, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthBuffer);

	// check frame buffer for errors
	GLenum e = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	if (e != GL_FRAMEBUFFER_COMPLETE) {
    	lg::log() << "frame buffer creation failed: " << e;
		return false;
	} else {
		lg::log(lg::info) << "fbo created successfully";
	}

    // clean up
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindRenderbufferEXT(GL_RENDERBUFFER, 0);
	
	return true;
}

void Renderer::displayFBO() const {
	// Clear the screen
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(m_fullScreenShaderId);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_colorBuffer);
	// Set our "renderedTexture" sampler to use Texture Unit 0
	glUniform1i(m_fullScreenTextId, 0);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_fullScreenVertexBuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	// Draw the triangles !
	glDrawArrays(GL_TRIANGLES, 0, 6); // 2*3 indices starting at 0 -> 2 triangles
	glDisableVertexAttribArray(0);
}