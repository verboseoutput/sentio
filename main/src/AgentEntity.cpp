#include "AgentEntity.h"

#include <thread>

#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include "Log.h"

AgentEntity::AgentEntity() {
}

AgentEntity::~AgentEntity() {
}

void AgentEntity::setupInput(int width, int height) {
	m_width = width;  
	m_height = height;
	
	float FoV = 45.0;
	float aspectRatio = float(width) / float(height);
    m_projectionMatrix = glm::perspective(FoV, aspectRatio, 0.1f, 100.0f);
}

void AgentEntity::computeMatrices() {
    glm::vec3 direction(
		cos(m_verticalAngle) * sin(m_horizontalAngle),
		sin(m_verticalAngle),
		cos(m_verticalAngle) * cos(m_horizontalAngle)
	);

	glm::vec3 right = glm::vec3(
		sin(m_horizontalAngle - glm::half_pi<glm::float32>()),
		0,
		cos(m_horizontalAngle - glm::half_pi<glm::float32>())
	);

	glm::vec3 up = glm::cross(right, direction);

    m_viewMatrix = glm::lookAt(
		m_position,
		m_position + direction,
		up
	);
}

void AgentEntity::run(std::atomic<bool>& running) {
	while(running) {
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		lg::log(lg::info) << "agent running";
	}
	lg::log(lg::info) << "agent while done";
}