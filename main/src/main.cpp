#include <iostream>
#include <fstream>
#include <cmath>

// make sure you include your openGL loader before everything else
#include <glbinding/gl/gl.h>
#include <glbinding/Binding.h>
#include <SDL.h>

#include "Renderer.h"
#include "World.h"
#include "Log.h"
#include "WorldOptions.h"

using namespace gl;
int main(int argc, char* argv[]) {
	// set logging granularity (options are: info, warning, error) info is default
	// lg::log.setMinLevel(lg::warning);
	
	// logging is meant to report information back to the user, not just for debugging
	// so any logging statement should make sense as part of a release build

	// create window and openGL context
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		lg::log() << "SDL_Init Error: " << SDL_GetError();
		return EXIT_FAILURE;
	}

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	// TODO: application screen size should be divorced from openGL render size
    int screenWidth = 512;
    int screenHeight = 512;
	SDL_Window* window = SDL_CreateWindow("Sentio", 
	                                    SDL_WINDOWPOS_UNDEFINED, 
										SDL_WINDOWPOS_UNDEFINED, 
										screenWidth, screenHeight, 
										SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);

    if(window == nullptr) {
        lg::log() << "window could not be created: " << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    // Create an OpenGL context associated with the window.
    SDL_GLContext glcontext = SDL_GL_CreateContext(window);
	// Initialize our openGL loader (lazy initialization)
	glbinding::Binding::initialize(false);

	/**
	 * TODO:
	 * display a GUI where test options are selectable including
	 *  - what world/test we're running
	 *  - which agent should run it
	 *  - render options
	 * 
	 * After which, the user will hit the run button and the test will be run
	 */

	WorldOptions opt;
	opt.renderWidth = 512;
	opt.renderHeight = 512;

	// create world
	World world(opt);
	if(!world.init()) {
		lg::log() << "world failed to initialize";
		return EXIT_FAILURE;
	} else {
		lg::log(lg::info) << "world loaded";
	}

	// MAIN LOOP
	// the test is run
	world.run(window);

    SDL_GL_DeleteContext(glcontext);  
    SDL_DestroyWindow(window);
	
	window = nullptr;

    atexit(SDL_Quit);
    return 0;
}

