#include "World.h"

#ifdef LINUX
#include <unistd.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif
#include <thread>
#include <atomic>

#include <SDL.h>
#include <assimp/Importer.hpp>
#include <glbinding/gl/gl.h>
#include <glm/gtc/constants.hpp>

#include "constants.h"
#include "connect.h"
#include "Renderer.h"
#include "Texture.h"
#include "Model.h"
#include "Entity.h"
#include "AgentEntity.h"
#include "GraphicsComponent.h"
#include "Log.h"

World::~World() {}

// Parse file defining object mesh needed, textures, World object properties
bool World::loadFromScript(const char* scriptName) {
    // TODO

	return true;
}

bool World::init() {
	// setup our agent entity
	m_agentEntity.setPosition(glm::vec3(0.0, 0.0, 0.0));
	m_agentEntity.setupInput(m_options.renderWidth, m_options.renderHeight);
	
	// setup the render object
	if (!m_renderer.init(m_agentEntity)) {
		lg::log() << "renderer failed to initialize\n";
		return false;
	}

    // import object (mesh, material, animation, etc) data
    m_models["tile"] = std::unique_ptr<Model>(new Model());
    m_models["tile"]->setPath("tile.fbx");

	m_models["monkey"] = std::unique_ptr<Model>(new Model());
	m_models["monkey"]->setPath("monkey.fbx");

	std::unique_ptr<Assimp::Importer>importer(new Assimp::Importer());
	for (auto const& obj : m_models)
	{
		if (!obj.second->loadModelData(*importer)) {
			lg::log() << "failed to load object data: " << obj.first;
			return false;
		} else {
			obj.second->setupRenderBuffer();
		}
	}

	// import texture data
	m_textures["redAndBlue"] = std::unique_ptr<Texture>(new Texture());
	m_textures["redAndBlue"]->setFileName("redAndBlue.png");

	for(auto const& texture : m_textures) {
		if(!texture.second->generateTexture()) {
			lg::log(lg::warning) << "failed to generate texture: " << texture.first;
		}
	}
	
	// associate object and texture data with each entity object
	if (!createEntities()) {
		lg::log() << "failed to create all objects needed for the World\n";
		return false;
	}

	// successfully inititalized World
	return true;
}

// The Main loop of our 3D world
void World::run(SDL_Window* window) {
	if(window == nullptr) {
	 	lg::log() << "window does not exist!";
	 	return;
	}

	std::atomic<bool> running(true);

	// resources shared between threads
	// the visual information
	unsigned char* visualBuffer = 
		new unsigned char[m_options.renderWidth * m_options.renderHeight * 4];
	// the movement commands

	// start agent
	std::thread agentThread(&AgentEntity::run, &m_agentEntity, std::ref(running));

	// fixed timestep to keep things simple
	const unsigned int FRAMES_PER_SECOND = 25;
	const unsigned int TICKS_PER_FRAME = 1000 / FRAMES_PER_SECOND;

	int missedFrames = 0;
	int nextFrameTick = SDL_GetTicks();
	int sleepTime;
	SDL_Surface *screen; 
	SDL_Surface* agentView; 
	while(running) {
		lg::log(lg::info) << "world running";

		// HANDLE EVENTS
		running = handleEvents();

		// UPDATE WORLD AND AGENT STATE
		m_renderer.setupFrame();
		m_agentEntity.update(TICKS_PER_FRAME);
		m_agentEntity.computeMatrices();
		for(auto const& entity : m_entities) {
			entity->update(TICKS_PER_FRAME);
		}
		glBindFramebuffer(gl::GL_FRAMEBUFFER, 0);

		// TRANSFER PIXEL DATA TO THE CPU
		updateHostArray(visualBuffer, m_options.renderWidth, m_options.renderHeight);
		
		// DISPLAY CURRENT VIEW
		bool showAgentView = true;
		if(showAgentView) { // from the CPU
			displayVisualBuffer(visualBuffer, window);
			SDL_UpdateWindowSurface(window);
		} else { // from the GPU
			m_renderer.displayFBO();
			SDL_GL_SwapWindow(window);
		}

		nextFrameTick += TICKS_PER_FRAME;
		sleepTime = nextFrameTick - SDL_GetTicks();
		if (sleepTime >= 0) {
			mySleep(sleepTime);
		} else {
			missedFrames++;
			lg::log(lg::warning) << "missed a frame by " << (-1*sleepTime) << " milliseconds";
			lg::log(lg::warning) << "Missed Frames: " << missedFrames;
			
			if (missedFrames > FRAMES_PER_SECOND) // completely arbitrary
			{
				lg::log() << "TOO MANY MISSED FRAMES";
				running = false;
			}
			nextFrameTick = SDL_GetTicks();
		}
	}

	lg::log(lg::info) << "world while loop done";
	agentThread.join();
	lg::log(lg::info) << "agent thread joined";
}

void World::cleanup() {
	// TODO
}

bool World::createEntities() {
	std::shared_ptr<GraphicsComponent> monkeyGraphicsComponent =
		std::make_shared<GraphicsComponent>(
			m_renderer,
			*(m_models["monkey"]),
			*(m_textures["redAndBlue"]));

	std::unique_ptr<Entity> monkeyX(new Entity());
	monkeyX->setPosition(glm::vec3(2, 0, 6));
	monkeyX->setScale(glm::vec3(1.0));
	monkeyX->setRotation(glm::vec3(glm::half_pi<glm::float32>(), glm::pi<glm::float32>(), 0.0));
	monkeyX->addComponent(Component::Type::graphics, monkeyGraphicsComponent);

	std::unique_ptr<Entity> monkeyY(new Entity());
	monkeyY->setPosition(glm::vec3(-2, 0, 4));
	monkeyY->setScale(glm::vec3(1.0));
	monkeyY->setRotation(glm::vec3(glm::half_pi<glm::float32>(), glm::pi<glm::float32>(), 0.0));
	monkeyY->addComponent(Component::Type::graphics, monkeyGraphicsComponent);
	
	m_entities.push_back(std::move(monkeyX));
	m_entities.push_back(std::move(monkeyY));

	return true;
}

void World::mySleep(Uint32 msecs) {
#ifdef LINUX
    usleep(msecs * 1000);   // usleep takes sleep time in us (1 millionth of a second)
#endif
#ifdef WINDOWS
    Sleep(msecs);
#endif
}

bool World::handleEvents() {
	SDL_Event event;
	while (SDL_PollEvent(&event) != 0) {
		if (event.type == SDL_QUIT) {
			return false;
		}
		else if (event.type == SDL_KEYUP) {
			if(event.key.keysym.sym == SDLK_ESCAPE) {
				return false;
			}
		}
	}
	return true;
}

void World::displayVisualBuffer(unsigned char* buffer, SDL_Window* window) {
	#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    const uint32_t R_MASK = 0xff000000;
    const uint32_t G_MASK = 0x00ff0000;
    const uint32_t B_MASK = 0x0000ff00;
    const uint32_t A_MASK = 0x000000ff;
#else
    const uint32_t R_MASK = 0x000000ff;
    const uint32_t G_MASK = 0x0000ff00;
    const uint32_t B_MASK = 0x00ff0000;
    const uint32_t A_MASK = 0xff000000;
#endif
	
	SDL_Surface* surface = SDL_CreateRGBSurfaceFrom(
		buffer,	//pixels
		m_options.renderWidth,		// width
		m_options.renderHeight,		// height
		32,			// depth
		m_options.renderWidth*4,		// pitch
		R_MASK,		// r mask
		G_MASK,		// g mask
		B_MASK,		// b mask
		A_MASK);	// a mask

	if(surface == nullptr) {
		lg::log() << "create rgb surface from error: " << SDL_GetError() << std::endl;
		return;
	}

	SDL_BlitSurface(surface, NULL, SDL_GetWindowSurface(window), NULL);
	
}