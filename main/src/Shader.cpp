#include <iostream>
#include <fstream>
#include "constants.h"
#include "shader.h"
#include "Log.h"

using namespace gl;

gl::GLuint compileShaderProgram(const char* vertexFilePath, const char* fragmentFilePath) {
    lg::log(lg::info) << "loading shaders...";

    // Create the shaders
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string vertexShaderCode;
	std::ifstream vertexShaderStream(SHADER_DIR + vertexFilePath, std::ios::in);
	if (vertexShaderStream.is_open())
    {
		std::string line = "";
		while (getline(vertexShaderStream, line))
        {
        	vertexShaderCode += "\n" + line;
        }
        vertexShaderStream.close();
	}
	else
    {
		lg::log() << "failed opening vertex shader file: " << SHADER_DIR << vertexFilePath;
		return -1;
	}

	// Read the Fragment Shader code from the file
	std::string fragmentShaderCode;
	std::ifstream fragmentShaderStream(SHADER_DIR + fragmentFilePath, std::ios::in);
	if (fragmentShaderStream.is_open())
    {
		std::string Line = "";
		while (getline(fragmentShaderStream, Line))
        {
			fragmentShaderCode += "\n" + Line;
        }
        fragmentShaderStream.close();
        lg::log(lg::info) << "done loading shaders";
	}
	else
    {
		lg::log() << "filed opening fragment shader file: " << SHADER_DIR << fragmentFilePath;
		return -1;
	}

	GLboolean result;
	int infoLogLength = 0;

	// Compile Vertex Shader
	lg::log(lg::info) << "compiling vertex shader: " << vertexFilePath << "...";
	char const* vertexSourcePointer = vertexShaderCode.c_str();
	glShaderSource(vertexShaderID, 1, &vertexSourcePointer, NULL);
	glCompileShader(vertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		GLchar* vertexShaderErrorMessage = (GLchar*)malloc(sizeof(GLchar)*infoLogLength);
		glGetShaderInfoLog(vertexShaderID, infoLogLength, NULL, vertexShaderErrorMessage);
		lg::log() << "failed compiling shader: " << vertexShaderErrorMessage;
		return -1;
	} else {
        lg::log(lg::info) << "done compiling vertex shader";
    }

	// Compile Fragment Shader
    lg::log(lg::info) <<  "compiling shader: " << fragmentFilePath << "...";
	char const* fragmentSourcePointer = fragmentShaderCode.c_str();
	glShaderSource(fragmentShaderID, 1, &fragmentSourcePointer, NULL);
	glCompileShader(fragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0){
		GLchar* fragmentShaderErrorMessage = (GLchar*)malloc(sizeof(GLchar)*infoLogLength);
		glGetShaderInfoLog(fragmentShaderID, infoLogLength, NULL, fragmentShaderErrorMessage);
		lg::log() << "failed compiled shader: " << fragmentShaderErrorMessage;
		return -1;
	} else {
        lg::log(lg::info) << "done compiling fragment shader";
    }

	// Link the program
	lg::log(lg::info) << "linking program...";
	GLuint programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);
    lg::log(lg::info) << "done linking";

	// Check the program
    lg::log(lg::info) << "checking program...";
	glGetProgramiv(programID, GL_LINK_STATUS, &result);
	glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0){
		GLchar* programErrorMessage = (GLchar*)malloc(sizeof(GLchar)*infoLogLength);
		glGetProgramInfoLog(programID, infoLogLength, NULL, programErrorMessage);
		lg::log() << programErrorMessage;
		return -1;
	} else {
        lg::log(lg::info) << "done checking";
    }

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

    return programID;
}