#include "Texture.h"

#include <iostream>
#include <string.h>

#include <glbinding/gl/types.h>
#include <glbinding/gl/functions.h>
#include <glbinding/gl/gl.h>
#include <SOIL.h>

#include "constants.h"
#include "Log.h"

using namespace  gl;
bool Texture::generateTexture() {
    if(m_fileName.empty()) {
        lg::log() << "file name not set yet";
		return false;
    }

	// generate texture
	glGenTextures(1, &m_textureHandle);
	glBindTexture(GL_TEXTURE_2D, m_textureHandle);

	// load image data (can't use soil auto load b/c uses deprecated openGL)
	int width, height;
	unsigned char* image = SOIL_load_image((TEXTURE_DIR+m_fileName).c_str(),
		 &width, &height, 0, SOIL_LOAD_RGB);
	
	if(image == 0) {
		lg::log() << "failed to load image " <<  m_fileName << ": " << SOIL_last_result();
		return false;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, 
		GL_RGB, GL_UNSIGNED_BYTE, image);
	
	SOIL_free_image_data(image);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	return true;
}