#include "Model.h"

#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glbinding/gl/gl.h>

#include "constants.h"
#include "Log.h"

using namespace gl;
bool Model::loadModelData(Assimp::Importer& importer) {
	// TODO verify postprocessing that we want
	const aiScene* scene = importer.ReadFile( OBJECT_DIR+m_path, 
        aiProcess_CalcTangentSpace       | 
        aiProcess_Triangulate            |
        aiProcess_JoinIdenticalVertices  |
        aiProcess_SortByPType);
  
  	// If the import failed
	if (!scene)
	{
    	lg::log() << "failed to load scene with obj at: " << m_path;
    	return false;
	}

    if (scene->HasMeshes())
    {
		if(scene->mNumMeshes > 1) {
			lg::log(lg::warning) << "unsupported number of meshes: " << scene->mNumMeshes;
		} else {
			setMeshData(scene->mMeshes[0]);
		}
    }

	if (scene->HasAnimations()){
		lg::log(lg::warning) << m_path << " has animations. Animations unsupported";
	}
	
	if (scene->HasMaterials()){
		lg::log(lg::warning) << m_path << " has materials. Materials unsupported";
	}
	
	return true;
}

void Model::setMeshData(const aiMesh* mesh) {

    for(int i=0; i<mesh->mNumVertices; i++) {
        glm::vec3 vertex(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
        glm::vec2 uv(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
        glm::vec3 normal(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);

        indexedVertices.push_back(vertex);
        indexedUVs.push_back(uv);
        indexedNormals.push_back(normal);
    }

    for(int i=0; i<mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        for(int j=0; j<face.mNumIndices; j++) {
            indices.push_back(face.mIndices[j]);
        }
    }
}

void Model::setupRenderBuffer() {
    glGenBuffers(Model::Data::COUNT, m_bufferData);

    // vector data
    glBindBuffer(GL_ARRAY_BUFFER, m_bufferData[VERTEX]);
    glBufferData(
		GL_ARRAY_BUFFER,
		indexedVertices.size() * sizeof(glm::vec3),
		&indexedVertices[0],
		GL_STATIC_DRAW);

    // uv data
    glBindBuffer(GL_ARRAY_BUFFER, m_bufferData[UV]);
    glBufferData(
		GL_ARRAY_BUFFER,
		indexedUVs.size() * sizeof(glm::vec2),
		&indexedUVs[0],
		GL_STATIC_DRAW);


    // normal data
    glBindBuffer(GL_ARRAY_BUFFER, m_bufferData[NORMAL]);
    glBufferData(
		GL_ARRAY_BUFFER,
		indexedNormals.size() * sizeof(glm::vec3),
		&indexedNormals[0],
		GL_STATIC_DRAW);


    // element data
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bufferData[ELEMENT]);
    glBufferData(
		GL_ELEMENT_ARRAY_BUFFER,
		indices.size() * sizeof(unsigned short),
		&indices[0],
		GL_STATIC_DRAW);
}

GLuint Model::getBuffer(Data data) {
    return m_bufferData[data];
}


int Model::getSize() {
    return indices.size();
}