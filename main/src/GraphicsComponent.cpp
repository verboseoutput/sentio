#include "GraphicsComponent.h"

#include <glbinding/gl/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "AgentEntity.h"
#include "Entity.h"
#include "Renderer.h"
#include "Model.h"
#include "Texture.h"

using namespace gl;
void GraphicsComponent::update(Entity& entity, unsigned int ticksPerFrame) {

	// perform any animation

    glm::mat4 scaleMatrix = glm::scale(
		glm::mat4(1.0f),
		entity.getScale());
	glm::mat4 rotateMatrix = glm::rotate(
		glm::mat4(1.0f),
		entity.getXRotation(),
		glm::vec3(1, 0, 0));
	rotateMatrix = glm::rotate(
		rotateMatrix,
		entity.getYRotation(),
		glm::vec3(0, 1, 0));
	rotateMatrix = glm::rotate(
		rotateMatrix,
		entity.getZRotation(),
		glm::vec3(0, 0, 1));
	glm::mat4 translateMatrix = glm::translate( // sets position in worldspace
		glm::mat4(1.0f),
		entity.getPosition());
	glm::mat4 modelMatrix = translateMatrix * rotateMatrix * scaleMatrix;
	glm::mat4 MVP = m_renderer.getMVP(modelMatrix);

	shader_ids shaderIds = m_renderer.getShaderIds();
	glUniformMatrix4fv(shaderIds.matrixId, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(shaderIds.modelMatrixId, 1, GL_FALSE, &modelMatrix[0][0]);

	// bind current game obj texture
	glBindTexture(GL_TEXTURE_2D, m_texture.getId());

	// first attribute : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_model.getBuffer(Model::Data::VERTEX));
	glVertexAttribPointer(
		0,			// attribute 0, arbitrary, but must match glEnableVertexAttribArray
		3,			// size
		GL_FLOAT,	// type
		GL_FALSE,	// normalize?
		0,			// stride
		(void*)0);	// array buff offset

	// second attribute : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_model.getBuffer(Model::Data::UV));
	glVertexAttribPointer(
		1,			
		2,			
		GL_FLOAT,
		GL_FALSE,
		0,	
		(void*)0);

	// third attribute : normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, m_model.getBuffer(Model::Data::NORMAL));
	glVertexAttribPointer(
		2,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0);

	// index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_model.getBuffer(Model::Data::ELEMENT));

	// actually draw triangle(s)
	glDrawElements(
		GL_TRIANGLES,		// mode
		m_model.getSize(),	// count
		GL_UNSIGNED_SHORT,	// type
		(void*)0);			// offset

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}