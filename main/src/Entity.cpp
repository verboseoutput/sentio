#include <glbinding/gl/gl.h>

#include "Entity.h"

using namespace gl;
void Entity::setPosition(glm::vec3 pos) {
    m_position = pos;
}

void Entity::setScale(glm::vec3 scale) {
    m_scale = scale;
}

void Entity::setRotation(glm::vec3 rotation) {
    m_rotation = rotation;
}

glm::vec3 Entity::getPosition() {
    return m_position;
}

glm::vec3 Entity::getScale() {
    return m_scale;
}

float Entity::getXRotation() {
    return m_rotation.x;
}

float Entity::getYRotation() {
    return m_rotation.y;
}

float Entity::getZRotation() {
    return m_rotation.z;
}

void Entity::addComponent(Component::Type type, std::shared_ptr<Component> component) {
    m_components[type] = component;
}

void Entity::update(unsigned int ticksPerFrame) {
    for(auto component : m_components) {
        component.second->update(*this, ticksPerFrame);
    }
}

void Entity::rotate(glm::vec3 rotateVector) {
    m_rotation = m_rotation + rotateVector;
}