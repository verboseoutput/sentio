# Sentio

The goal of this project is to provide a place to test computer vision algorithms in a simulated enviroment.
To that end this project includes a (very) simple graphics engine to create the 3D enviroment which renders
an adjustably sized set of pixel information (rgb values) to a location in memory. Using only this visual
information, an agent running on a seperate thread must navigate the 3D enviroment to accomplish a goal.

## Progress

**Current State**

Project successfully loads and renders a textured object.

**Next Goal**

Render the scene to an FBO which can be accessed by an agent.

## Requirements

1. The user can select a 3D environment for the agent to navigate
2. The user can select an agent
3. The environment must have a win condition
4. The agent must have access to a rendered 2D view of the world via a 'camera'
5. The agent must be able to move the 'camera' in order to adjust its view
6. The program should output information on the agents progress

## Building

### Supported Platforms

- Linux

### Dependencies

- SDL (2.0.5)
- SOIL
- assimp
- glbinding
- glm

The source of the external libraries necessary to build  are included as snapshots in the 
`lib` directory. In the future the build process will probably use a combination
of `find_package` and building the dependencies from source. I'd prefer to avoid
installing any dependecies to keep this project self contained and easy to clean
up.

### Compilation

This project uses [CMake](https://cmake.org "Cmake homepage") which can be used to generate Makefiles (Unix) or
Visual Studio solutions (Windows). The general process is:
```
cd /path/to/sentio/build
cmake ..
make
```

### output

The executable 'sentio' can be found in the `bin` directory.

